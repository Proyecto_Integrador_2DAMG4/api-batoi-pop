package com.g4.bpop;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.g4.bpop.pojos.BatoiPopCategorias;

@RestController
@RequestMapping("/categorias")
public class BatoiPopCategoriasController {
	@Autowired
	BatoiPopCategoriasService batoiPopCategoriasService;

	@GetMapping("")
	public List<BatoiPopCategorias> list() {
		return batoiPopCategoriasService.listAllCategorias();
	}

	@GetMapping("/{id}")
	public ResponseEntity<BatoiPopCategorias> get(@PathVariable Integer id) {
		try {
			BatoiPopCategorias cat = batoiPopCategoriasService.getCategorias(id);
			return new ResponseEntity<BatoiPopCategorias>(cat, HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<BatoiPopCategorias>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("")
	public ResponseEntity<?> add(@RequestBody BatoiPopCategorias cat) {
		batoiPopCategoriasService.saveCategorias(cat);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> update(@RequestBody BatoiPopCategorias cat, @PathVariable Integer id) {
		try {
			BatoiPopCategorias actualCat = batoiPopCategoriasService.getCategorias(id);
			actualCat.setName(cat.getName());
			batoiPopCategoriasService.saveCategorias(actualCat);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable Integer id) {
		try {
			batoiPopCategoriasService.deleteCategorias(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (EmptyResultDataAccessException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

}

