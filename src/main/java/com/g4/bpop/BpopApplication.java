package com.g4.bpop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BpopApplication {

	public static void main(String[] args) {
		SpringApplication.run(BpopApplication.class, args);
	}

}
