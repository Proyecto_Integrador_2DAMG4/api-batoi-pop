package com.g4.bpop;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.g4.bpop.pojos.BatoiPopUsuarios;

@Service
@Transactional
public class BatoiPopUsuariosService {
	@Autowired
	private BatoiPopUsuariosRepository batoiPopUsuariosRepository;

	public List<BatoiPopUsuarios> listAllUsuarios() {
		return batoiPopUsuariosRepository.findAll();
	}

	public BatoiPopUsuarios getUsuario(Integer id) {
		return batoiPopUsuariosRepository.findById(id).get();
	}

	public void saveUsuario(BatoiPopUsuarios usr) {
		batoiPopUsuariosRepository.save(usr);
	}
	
	public void deleteUsuario(Integer id) {
		batoiPopUsuariosRepository.deleteById(id);
	}

}