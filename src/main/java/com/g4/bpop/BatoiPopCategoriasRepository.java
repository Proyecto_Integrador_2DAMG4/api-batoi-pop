package com.g4.bpop;

import org.springframework.data.jpa.repository.JpaRepository;

import com.g4.bpop.pojos.BatoiPopCategorias;

public interface BatoiPopCategoriasRepository extends JpaRepository<BatoiPopCategorias, Integer> {

}
