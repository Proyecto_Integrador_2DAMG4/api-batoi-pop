package com.g4.bpop;

import org.springframework.data.jpa.repository.JpaRepository;

import com.g4.bpop.pojos.BatoiPopArticulos;

public interface BatoiPopArticulosRepository extends JpaRepository<BatoiPopArticulos, Integer> {

}