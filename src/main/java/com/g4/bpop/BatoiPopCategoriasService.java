package com.g4.bpop;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.g4.bpop.pojos.BatoiPopCategorias;

@Service
@Transactional
public class BatoiPopCategoriasService {
	@Autowired
	private BatoiPopCategoriasRepository batoiPopCategoriasRepository;

	public List<BatoiPopCategorias> listAllCategorias() {
		return batoiPopCategoriasRepository.findAll();
	}

	public BatoiPopCategorias getCategorias(Integer id) {
		return batoiPopCategoriasRepository.findById(id).get();
	}

	public void saveCategorias(BatoiPopCategorias cat) {
		batoiPopCategoriasRepository.save(cat);
	}
	
	public void deleteCategorias(Integer id) {
		batoiPopCategoriasRepository.deleteById(id);
	}

}