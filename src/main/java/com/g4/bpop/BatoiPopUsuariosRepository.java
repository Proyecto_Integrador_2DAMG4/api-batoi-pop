package com.g4.bpop;

import org.springframework.data.jpa.repository.JpaRepository;

import com.g4.bpop.pojos.BatoiPopUsuarios;

public interface BatoiPopUsuariosRepository extends JpaRepository<BatoiPopUsuarios, Integer> {

}
