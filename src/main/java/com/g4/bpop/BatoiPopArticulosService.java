package com.g4.bpop;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.g4.bpop.pojos.BatoiPopArticulos;

@Service
@Transactional
public class BatoiPopArticulosService {
	@Autowired
	private BatoiPopArticulosRepository batoiPopArticulosRepository;

	public List<BatoiPopArticulos> listAllArticulos() {
		return batoiPopArticulosRepository.findAll();
	}

	public BatoiPopArticulos getArticulo(Integer id) {
		return batoiPopArticulosRepository.findById(id).get();
	}

	public void saveArticulo(BatoiPopArticulos art) {
		batoiPopArticulosRepository.save(art);
	}
	
	public void deleteArticulo(Integer id) {
		batoiPopArticulosRepository.deleteById(id);
	}

}