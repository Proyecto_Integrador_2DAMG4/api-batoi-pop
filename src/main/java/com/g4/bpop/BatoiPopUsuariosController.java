package com.g4.bpop;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.g4.bpop.pojos.BatoiPopUsuarios;

@RestController
@RequestMapping("/usuarios")
public class BatoiPopUsuariosController {
	
	@Autowired
	BatoiPopUsuariosService batoiPopUsuariosService;

	@GetMapping("")
	public List<BatoiPopUsuarios> list() {
		return batoiPopUsuariosService.listAllUsuarios();
	}

	@GetMapping("/{id}")
	public ResponseEntity<BatoiPopUsuarios> get(@PathVariable Integer id) {
		try {
			BatoiPopUsuarios usr = batoiPopUsuariosService.getUsuario(id);
			return new ResponseEntity<BatoiPopUsuarios>(usr, HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<BatoiPopUsuarios>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("")
	public ResponseEntity<?> add(@RequestBody BatoiPopUsuarios usr) {
		batoiPopUsuariosService.saveUsuario(usr);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> update(@RequestBody BatoiPopUsuarios usr, @PathVariable Integer id) {
		try {
			BatoiPopUsuarios actualUsr = batoiPopUsuariosService.getUsuario(id);
			actualUsr.setPassword(usr.getPassword());
			batoiPopUsuariosService.saveUsuario(actualUsr);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable Integer id) {
		try {
			batoiPopUsuariosService.deleteUsuario(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (EmptyResultDataAccessException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

}

